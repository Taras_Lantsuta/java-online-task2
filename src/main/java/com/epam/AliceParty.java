package com.epam;

import java.util.Scanner;

public class AliceParty {

    public static int countPeopleReached(boolean mas[]) {
        int counter = 0;
        for (int i = 1; i < mas.length; i++)
            if (mas[i])
                counter++;
        return counter;
    }

    public static boolean rumorSpreaded(boolean mas[]) {
        for (int i = 1; i < mas.length; i++)
            if (!mas[i])
                return false;
        return true;
    }

    public static void main(String[] args) {

        final int numOfAttempts = 100;
        System.out.println("How much guests come to party? Enter number bigger then 2");
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int countRumorFullSpreaded = 0;
        int reachedPeople = 0;

        for (int i = 0; i < numOfAttempts; i++) {
            boolean people[] = new boolean[n];
            people[1] = true;
            boolean heardAlready = false;
            int nextGuest = -1;
            int currentGuest = 1;

            while (!heardAlready) {
                nextGuest = 1 + (int) (Math.random() * (n - 1));
                if (nextGuest == currentGuest) {
                    while (nextGuest == currentGuest)
                        nextGuest = 1 + (int) (Math.random() * (n - 1));
                }
                if (people[nextGuest]) {
                    if (rumorSpreaded(people))
                        countRumorFullSpreaded++;
                    reachedPeople = reachedPeople + countPeopleReached(people);
                    heardAlready = true;
                }
                people[nextGuest] = true;
                currentGuest = nextGuest;
            }
        }
        System.out.println("Probability that everyone at the party (except Alice) will hear the rumor before it " +
                "stops propagating is " + (double) countRumorFullSpreaded / numOfAttempts);
        System.out.println("Expected number of people to hear the rumor is: " + reachedPeople / numOfAttempts);
    }
}
